import './App.css';
import { useState } from 'react';
import ItemCard from './components/item-card'

function App() {
  const [items, setItems] = useState([]);
  const [item, setItem] = useState('');
  const [showErrorMsg, setShowErrorMsg] = useState(false);

  function deleteItem (index) {
    const newItems = items.filter(i => items.indexOf(i) !== index);
    setItems(newItems);
  }

  function addItem () {
    if(item) {
      setItems([...items, item]);
      setItem('');
    } else {
      setShowErrorMsg(true);
    }
  }

  function handleChange(e) {
    const { value } = e.target;
    
    setItem(value);
    setShowErrorMsg(false);
  }

  return (
    <div className="App">
    <h3>To do List</h3>
      <div className="addItem">
          <input
            className="itemInput"
            placeholder="Insert task to add to list"
            data-testid="item-input"
            value={item}
            onChange={handleChange}
            />
          <button onClick={addItem} data-testid="Add">Add</button>
          {showErrorMsg &&
            <p className="errorMsg">You cannot add empty value!</p>
          }
      </div>

      <div className="App-body" data-testid="itemlist">
        {items.length > 0 && items.map((i, index) => (
          <ItemCard key={index} item={i} delete={() => deleteItem(index)}/>
        ))}
      </div>
    </div>
  );
}

export default App;
