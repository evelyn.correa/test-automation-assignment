/* eslint-disable no-undef */
/* eslint-disable jest/valid-expect */
describe("Main Page", () => {
    it("should render correctly", () => {
        cy.visit('http://localhost:3000');

        cy.contains('To do List');

        cy.get('.addItem').contains('Add');
    })

    it('should show error on trying to add empty', () => {
        cy.get('.addItem').contains('Add').click();
        cy.get('.errorMsg').contains('You cannot add empty value!');
    })

    it('should add task correctly', () => {
        cy.get('.itemInput').type('First task');
        cy.get('button').click();
    })

    it('should delete task', () => {
        cy.get('.itemcard-button').click();
    })
})